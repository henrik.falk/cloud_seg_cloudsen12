import os
import torch
import cloudsen12
import rasterio as rio
import numpy as np
import matplotlib.pyplot as plt
from dotenv import load_dotenv
from torchvision.transforms.functional import pad

from ssl_pretext_experiments.segmodel_module import LitSegModel

loaded_model = LitSegModel.load_from_checkpoint("../ssl_pretext_experiments/bestmodel/AKTL"
                                                "/regnet_y_saa_sza_e50_BOA-931.ckpt")
# loaded_model = cloudsen12.UnetMobV2().model
loaded_model.eval()
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
loaded_model.to(device)


def inference_sentinel_image(image_path):
    with rio.open(image_path) as src:
        bands_data = [src.read(band_index) for band_index in range(1, 14)]
    normalized_sza = 0.717  # SZA here
    normalized_saa = 0.575  # SAA here
    normalized_elevation_array = np.full((509, 509), normalized_sza, dtype=np.float32)
    normalized_azimuth_array = np.full((509, 509), normalized_saa, dtype=np.float32)
    bands_data.append(normalized_elevation_array)  # comment out if without SZA and SAA
    bands_data.append(normalized_azimuth_array)  # comment out if without SZA and SAA
    input_image = np.stack(bands_data, axis=-1)
    input_tensor = torch.tensor(input_image, dtype=torch.float32).permute(2, 0, 1)
    input_tensor /= 10000.0
    input_tensor = pad(input_tensor, padding=[1, 1, 2, 2])
    input_tensor = input_tensor.to(device)
    input_batch = input_tensor.unsqueeze(0)
    with torch.no_grad():
        output = loaded_model(input_batch)
    _, predicted_class = torch.max(output, 1)
    segmentation_result = predicted_class.squeeze().cpu().numpy()
    return segmentation_result

load_dotenv()
CONTENT_CLOUDSEN12_PATH = os.getenv("CONTENT_CLOUDSEN12_PATH")
sentinel_image_path = CONTENT_CLOUDSEN12_PATH+"/high/ROI_0097/20190225T142751_20190225T143548_T19JFK/S2L1C.tif"
result = inference_sentinel_image(sentinel_image_path)
plt.imshow(result)
plt.show()

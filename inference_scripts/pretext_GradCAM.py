import os
import torch
import rasterio as rio
import numpy as np
import matplotlib.pyplot as plt
from dotenv import load_dotenv
from torchvision.transforms.functional import pad
from ssl_pretext_experiments.premodel_module import LitPretextModel

loaded_model = LitPretextModel.load_from_checkpoint("../ssl_pretext_experiments/bestmodel"
                                                "/regnet_y_max_pretext_noaug_e25.ckpt")
encoder = loaded_model.model
encoder.eval()
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
encoder.to(device)

feature_maps = None
gradients = None

def get_features_hook(module, input, output):
    global feature_maps
    feature_maps = output

def get_gradients_hook(module, grad_input, grad_output):
    global gradients
    gradients = grad_output[0]

target_layer = encoder.segmentation_head
target_layer.register_forward_hook(get_features_hook)
target_layer.register_full_backward_hook(get_gradients_hook)

def inference_sentinel_image(image_path, model):
    with rio.open(image_path) as src:
        bands_data = [src.read(band_index) for band_index in range(1, 14)]
    input_image = np.stack(bands_data, axis=-1)
    input_tensor = torch.tensor(input_image, dtype=torch.float32).permute(2, 0, 1)
    input_tensor /= 10000.0
    input_tensor = pad(input_tensor, padding=[1, 1, 2, 2])
    input_tensor = input_tensor.to(device)
    input_batch = input_tensor.unsqueeze(0)
    output = model(input_batch)
    return output, input_image

load_dotenv()
CONTENT_CLOUDSEN12_PATH = os.getenv("CONTENT_CLOUDSEN12_PATH")
sentinel_image_path = CONTENT_CLOUDSEN12_PATH + "/high/ROI_0032/20190713T032541_20190713T033534_T49TFH/S2L1C.tif"
result, sentinel_image = inference_sentinel_image(sentinel_image_path, encoder)
result.requires_grad_(True)
pred = result.argmax(dim=1)
batch, num_classes, height, width = result.shape
one_hot_output = torch.zeros(batch, num_classes, height, width).to(device)
one_hot_output[:, :1, :, :] = 1
pred = pred.unsqueeze(1).to(device)
one_hot_output.scatter_(1, pred, 1)

loss = (result * one_hot_output).sum()
loss.backward()

if gradients is not None:
    weights = torch.mean(gradients, dim=[0, 2, 3], keepdim=True)
    grad_cam_map = torch.sum(weights * feature_maps, dim=1).squeeze(0)
    grad_cam_map = torch.relu(grad_cam_map)
    grad_cam_map = (grad_cam_map - grad_cam_map.min()) / (grad_cam_map.max() - grad_cam_map.min())
    grad_cam_map = grad_cam_map.cpu()
    plt.imshow(grad_cam_map.detach().numpy(), cmap='jet', interpolation='nearest')
    plt.colorbar()
    plt.axis("off")
    plt.title("Grad-CAM Heatmap")
    plt.show()

import cloudsen12
import matplotlib.pyplot as plt
import numpy as np
import rasterio as rio
from matplotlib.colors import ListedColormap

"""
Run inference on .jp2 images:
The model selection needs to be done in unetmobv2.py from the cloudsen library.
For SZA and SAA as input signals the following lines need to be added to module.py in the MaskayModule.
Paste the code at line 282 before IP = np.stack(IP, axis=0):

normalized_sza = 0.402 # SZA here
normalized_saa = 0.204 # SAA here
normalized_elevation_array = np.full((512, 512), normalized_sza, dtype=np.float32)
normalized_azimuth_array = np.full((512, 512), normalized_saa, dtype=np.float32)
IP.append(normalized_elevation_array)
IP.append(normalized_azimuth_array)
"""

S2files = cloudsen12.MaskayDict(
    path="/home/admin/Studium/Bachelorarbeit/data/inference_jp2_data_goe/",
    pattern="\.jp2$",
    full_names=True,
    recursive=True,
    sensor="Sentinel-2"
)
tensor = cloudsen12.TensorSat(**S2files.to_dict(), cache=True, align=False)
model = cloudsen12.UnetMobV2()

predictor = cloudsen12.Predictor(
    cropsize=512,
    overlap=32,
    device="cuda", #cpu
    quiet=False,
)

result = predictor.predict(model, tensor)
result.rio.to_raster("outtensor.tif")

with rio.open("outtensor.tif") as src:
    result = src.read() / 10000

sl = slice(0, 10980)

excluded_result = np.concatenate((result[0:2, sl, sl], result[3:4, sl, sl]), axis=0)
combined_result = np.argmax(excluded_result, axis=0)

viridis = plt.get_cmap('viridis', 4)
newcolors = viridis(np.linspace(0, 1, 4))
newcolors[2] = viridis(0.25)
custom_cmap = ListedColormap(newcolors)

fig, ax = plt.subplots(1, 1, figsize=(10, 10))
im = ax.imshow(combined_result, cmap=custom_cmap)
ax.set_title("Combined highest probability class")
# cbar = fig.colorbar(im, ax=ax, label='Class')

plt.subplots_adjust(bottom=0.15)
plt.show()

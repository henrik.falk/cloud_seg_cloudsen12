import numpy as np
import pytorch_lightning as pl
import segmentation_models_pytorch as smp
import torch
from pytorch_lightning.loggers import TensorBoardLogger
from torchvision.utils import make_grid
from torchmetrics.classification import f_beta, BinaryPrecision, BinaryRecall, BinarySpecificity, BinaryStatScores

mobilenet_SEGMODEL = smp.Unet(encoder_name="mobilenet_v2", encoder_weights=None, classes=4, in_channels=13)
# tensorboard_logger = TensorBoardLogger(save_dir="mobilenet_azimuth" + "_logs", name="mobilenet_azimuth_model",
#                                       default_hp_metric=False)
# torch.use_deterministic_algorithms(True, warn_only=True)

class CrossEntropyLoss(torch.nn.Module):
    def __init__(self):
        super().__init__()

    def forward(self, input, target):
        target = target.type(torch.long)
        BCE = torch.nn.functional.cross_entropy(input, target)
        return BCE


class LitSegModel(pl.LightningModule):
    def __init__(self, pretext_task="", layer=0, SEGMODEL=mobilenet_SEGMODEL):
        super().__init__()
        self.save_hyperparameters()
        self.model = SEGMODEL
        self.criterion = CrossEntropyLoss()
        self.fbeta = f_beta.BinaryFBetaScore(beta=2.0)
        self.re = BinaryRecall()
        self.pre = BinaryPrecision()
        self.spec = BinarySpecificity()
        self.stats = BinaryStatScores()
        self.bf2_scores = list()
        self.bpa_scores = list()
        self.bua_scores = list()
        self.boa_scores = list()
        self.pretext_task = pretext_task
        self.layer = layer

    def prepare_data(self) -> None:
        pass

    def forward(self, inputs):
        return self.model(inputs)

    def training_step(self, batch, batch_idx):
        inputs, target, _ = batch
        output = self.forward(inputs)
        loss = self.criterion(output, target)
        self.log("loss_train", loss.cuda(), prog_bar=True, logger=True, on_epoch=True, sync_dist=True)
        """# Add grid of batch images to tensorboard
        X_reshape = inputs[0, [3, 2, 1], :, :]
        grid = make_grid(X_reshape, nrow=4)
        tensorboard_logger.experiment.add_image("batch images", grid, self.global_step)
        X_reshape_shwdir = inputs[0, 14, 0, 0]
        tensorboard_logger.experiment.add_scalar("shadow direction", X_reshape_shwdir, self.global_step)"""
        return loss

    def validation_step(self, batch, batch_idx):
        inputs, target, _ = batch
        output = self.forward(inputs)
        loss = self.criterion(output, target)
        self.log("loss_val", loss.cuda(), prog_bar=True, logger=True, on_epoch=True, sync_dist=True)
        output = output.squeeze()
        if output.shape.__len__() == 4:
            output_class = output.argmax(dim=1)
        else:
            output_class = output.argmax(dim=0)
            output_class = output_class.unsqueeze(0)
        target = target.type(torch.long)
        output_class = torch.where(output_class == 2, torch.ones_like(output_class), output_class)
        target = torch.where(target == 2, torch.ones_like(target), target)
        output_class = torch.where(output_class == 3, torch.zeros_like(output_class), output_class)
        target = torch.where(target == 3, torch.zeros_like(target), target)
        self.update_BF2score(output_class, target)
        self.update_BPAscore(output_class, target)
        self.update_BUAscore(output_class, target)
        self.update_BOAscore(output_class, target)
        return loss

    def on_validation_epoch_end(self):
        self.log(name="BF2score_test", value=self.compute_BF2score().cuda(), prog_bar=True, logger=True, on_epoch=True, sync_dist=True)
        self.log(name="BPAscore_test", value=self.compute_BPAscore().cuda(), prog_bar=True, logger=True, on_epoch=True, sync_dist=True)
        self.log(name="BUAscore_test", value=self.compute_BUAscore().cuda(), prog_bar=True, logger=True, on_epoch=True, sync_dist=True)
        self.log(name="BOAscore_test", value=self.compute_BOAscore().cuda(), prog_bar=True, logger=True, on_epoch=True, sync_dist=True)
        self.bf2_scores = list()
        self.bpa_scores = list()
        self.bua_scores = list()
        self.boa_scores = list()

    def update_BF2score(self, preds: torch.Tensor, target: torch.Tensor):
        assert preds.shape == target.shape
        for index in range(preds.shape[0]):
            stats = self.stats(preds[index], target[index])
            if stats[0] + stats[1] == 0 or stats[0] + stats[3] == 0:
                fbeta_result = torch.tensor(np.nan)
            else:
                fbeta_result = self.fbeta(preds[index], target[index])
            self.bf2_scores.append(fbeta_result)

    def compute_BF2score(self):
        bf2_scores_tensor = torch.Tensor(self.bf2_scores)
        gt_threshold = bf2_scores_tensor.gt(0.90)
        total = torch.sum(~torch.isnan(bf2_scores_tensor))
        return torch.sum(gt_threshold) / total * 100

    def update_BPAscore(self, preds: torch.Tensor, target: torch.Tensor):
        assert preds.shape == target.shape
        for index in range(preds.shape[0]):
            stats = self.stats(preds[index], target[index])
            if stats[0] + stats[3] == 0:
                re_result = torch.tensor(np.nan)
            else:
                re_result = self.re(preds[index], target[index])
            self.bpa_scores.append(re_result)

    def compute_BPAscore(self):
        bpa_scores_tensor = torch.Tensor(self.bpa_scores)
        gt_threshold = bpa_scores_tensor.gt(0.90)
        total = torch.sum(~torch.isnan(bpa_scores_tensor))
        return torch.sum(gt_threshold) / total * 100

    def update_BUAscore(self, preds: torch.Tensor, target: torch.Tensor):
        assert preds.shape == target.shape
        for index in range(preds.shape[0]):
            stats = self.stats(preds[index], target[index])
            if stats[0] + stats[1] == 0:
                pre_result = torch.tensor(np.nan)
            else:
                pre_result = self.pre(preds[index], target[index])
            self.bua_scores.append(pre_result)

    def compute_BUAscore(self):
        bua_scores_tensor = torch.Tensor(self.bua_scores)
        gt_threshold = bua_scores_tensor.gt(0.90)
        total = torch.sum(~torch.isnan(bua_scores_tensor))
        return torch.sum(gt_threshold) / total * 100

    def update_BOAscore(self, preds: torch.Tensor, target: torch.Tensor):
        assert preds.shape == target.shape
        for index in range(preds.shape[0]):
            stats = self.stats(preds[index], target[index])
            if stats[0] + stats[3] == 0 or stats[0] + stats[1] == 0:
                boa_result = torch.tensor(np.nan)
            else:
                re_result = self.re(preds[index], target[index])
                spec_result = self.spec(preds[index], target[index])
                boa_result = 0.5 * (re_result + spec_result)
            self.boa_scores.append(boa_result)

    def compute_BOAscore(self):
        boa_scores_tensor = torch.Tensor(self.boa_scores)
        return torch.nanmedian(boa_scores_tensor)

    def test_step(self, batch, batch_idx):
        inputs, target, _ = batch
        output = self.forward(inputs).squeeze()
        if output.shape.__len__() == 4:
            output_class = output.argmax(dim=1)
        else:
            output_class = output.argmax(dim=0)
            output_class = output_class.unsqueeze(0)
        target = target.type(torch.long)

        output_class = torch.where(output_class == 2, torch.ones_like(output_class), output_class)
        target = torch.where(target == 2, torch.ones_like(target), target)
        output_class = torch.where(output_class == 3, torch.zeros_like(output_class), output_class)
        target = torch.where(target == 3, torch.zeros_like(target), target)

        """        
        output_class = torch.where(output_class == 2, torch.zeros_like(output_class), output_class)
        target = torch.where(target == 2, torch.zeros_like(target), target)
        output_class = torch.where(output_class == 1, torch.zeros_like(output_class), output_class)
        target = torch.where(target == 1, torch.zeros_like(target), target)
        output_class = torch.where(output_class == 3, torch.ones_like(output_class), output_class)
        target = torch.where(target == 3, torch.ones_like(target), target)
        """

        self.update_BF2score(output_class, target)
        self.update_BPAscore(output_class, target)
        self.update_BUAscore(output_class, target)
        self.update_BOAscore(output_class, target)

    def on_test_epoch_end(self):
        self.log(name="BF2score_test", value=self.compute_BF2score().cuda(), prog_bar=True, logger=True, on_epoch=True, sync_dist=True)
        self.log(name="BPAscore_test", value=self.compute_BPAscore().cuda(), prog_bar=True, logger=True, on_epoch=True, sync_dist=True)
        self.log(name="BUAscore_test", value=self.compute_BUAscore().cuda(), prog_bar=True, logger=True, on_epoch=True, sync_dist=True)
        self.log(name="BOAscore_test", value=self.compute_BOAscore().cuda(), prog_bar=True, logger=True, on_epoch=True, sync_dist=True)

    def configure_optimizers(self) -> torch.optim.Optimizer:
        if self.pretext_task == "auto_unfreeze":
            param_groups = []
            base_lr = 0.0001
            for i, child in enumerate(list(self.model.encoder.children())[:2]):
                if i <= 0:
                    lr = 0.001
                else:
                    lr = base_lr
                layer_params = list(child.parameters())
                param_groups.append({'params': layer_params, 'lr': lr})
            encoder_params_end = []
            for child in list(self.model.encoder.children())[2:]:
                encoder_params_end.extend(list(child.parameters()))
            decoder_params = list(self.model.decoder.parameters())
            param_groups.append({'params': encoder_params_end + decoder_params, 'lr': 0.001})
            self.opt = torch.optim.Adam(param_groups)
        else:
            self.opt = torch.optim.AdamW(self.parameters(), lr=0.001)
        self.sch = {"scheduler": torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer=self.opt, mode="min", factor=0.1,
                                                                            patience=4, verbose=True), "frequency": 1,
                    "monitor": "loss_val"}
        return [self.opt], [self.sch]

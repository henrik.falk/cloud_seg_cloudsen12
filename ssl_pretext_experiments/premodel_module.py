import pytorch_lightning as pl
import segmentation_models_pytorch as smp
import torch
import torchvision


class SplitMseCosineLoss(torch.nn.Module):
    def __init__(self, tensorboard=None):
        super(SplitMseCosineLoss, self).__init__()
        self.tensorboard_logger = tensorboard
        self.mse_loss = torch.nn.MSELoss()
        self.cosine_loss = torch.nn.CosineEmbeddingLoss()

    def forward(self, y_hat, y):
        y_scalar = y[:, 0].unsqueeze(1)
        y_hat_scalar = y_hat[:, 0].unsqueeze(1)
        y_vector = y[:, 1:]
        y_hat_vector = y_hat[:, 1:]
        scalar_loss = self.mse_loss(y_hat_scalar, y_scalar)
        vector_loss = self.cosine_loss(y_hat_vector, y_vector, torch.ones(y_hat_vector.size(0)).cuda())
        total_loss = (scalar_loss * 10) + vector_loss
        return total_loss, scalar_loss, vector_loss


class LitPretextModel(pl.LightningModule):
    def __init__(self, in_channels=13, out_channels=3):
        super().__init__()
        self.model = smp.Unet(encoder_name="timm-regnety_006", encoder_weights=None, in_channels=in_channels,
                              classes=out_channels, activation=None)
        # self.encoder = self.model.encoder
        self.pool = torch.nn.AdaptiveAvgPool2d((1, 1))
        # self.fc = torch.nn.Linear(1280, out_channels)
        self.criterion = SplitMseCosineLoss()

    def forward(self, x):
        # x = self.encoder(x)[-1]
        x = self.model(x)
        x = self.pool(x)
        x = torch.flatten(x, 1)
        # x = self.fc(x)
        return x

    def training_step(self, batch, batch_idx):
        x, y, files = batch
        y_hat = self(x)
        loss, _, _ = self.criterion(y_hat, y)
        # Add grid of batch images to tensorboard
        """
        x_reshape = x[:, [3, 2, 1], :, :]
        grid = torchvision.utils.make_grid(x_reshape, nrow=4)
        self.tensorboard_logger.experiment.add_image("batch images", grid, self.global_step)
        self.tensorboard_logger.experiment.add_scalar("ROI", 10000 + int(files[0].split("_")[1]), self.global_step)
        y_shadow_radian = torch.atan2(y[0, 2], y[0, 1])
        y_shadow_degree = torch.rad2deg(y_shadow_radian % (2 * torch.pi))
        y_hat_shadow_radian = torch.atan2(y_hat[0, 2], y_hat[0, 1])
        y_hat_shadow_degree = torch.rad2deg(y_hat_shadow_radian % (2 * torch.pi))
        self.tensorboard_logger.experiment.add_scalar("y_shadow_degree", y_shadow_degree, self.global_step)
        self.tensorboard_logger.experiment.add_scalar("y_hat_shadow_degree", y_hat_shadow_degree, self.global_step)
        """
        self.log('train_loss', loss.cuda(), on_step=True, on_epoch=True, prog_bar=True, logger=True, sync_dist=True)
        return loss

    def validation_step(self, batch, batch_idx):
        x, y, _ = batch
        y_hat = self(x)
        loss, _, _ = self.criterion(y_hat, y)
        self.log('loss_val', loss.cuda(), on_step=True, on_epoch=True, prog_bar=True, logger=False, sync_dist=True)
        return loss

    def test_step(self, batch, batch_idx):
        x, y, files = batch
        y_hat = self(x)
        total_loss, scalar_loss, vector_loss = self.criterion(y_hat, y)
        self.log('test_total_loss', total_loss.cuda(), on_step=False, on_epoch=True, prog_bar=True, logger=True,
                 sync_dist=True)
        self.log('test_scalar_loss', scalar_loss.cuda(), on_step=False, on_epoch=True, prog_bar=True, logger=True,
                 sync_dist=True)
        self.log('test_vector_loss', vector_loss.cuda(), on_step=False, on_epoch=True, prog_bar=True, logger=True,
                 sync_dist=True)
        return {"test_total_loss": total_loss, "test_scalar_loss": scalar_loss, "test_vector_loss": vector_loss}

    def configure_optimizers(self):
        self.opt = torch.optim.AdamW(self.parameters(), lr=0.001)
        self.sch = {"scheduler": torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer=self.opt, mode="min", factor=0.1,
                                                                            patience=4, verbose=True), "frequency": 1,
                    "monitor": "loss_val"}
        return [self.opt], [self.sch]

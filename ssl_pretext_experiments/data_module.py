import math
import os
import warnings
import numpy as np
import pandas as pd
import pytorch_lightning as pl
import rasterio as rio
import torch
from pytorch_lightning.loggers import TensorBoardLogger
from torch.utils.data import DataLoader, IterableDataset, get_worker_info


# tensorboard_logger = TensorBoardLogger(save_dir="mobilenet_azimuth" + "_logs", name="mobilenet_azimuth_data",
#                                        default_hp_metric=False)
class NumpyTransforms:
    def __init__(self, augmentation, additional_input, pretext_task):
        self.augmentation = augmentation
        self.additional_input = additional_input
        self.pretext_task = pretext_task
        self.global_step = 0

    def pad_if_needed(self, image, mask):
        image = np.pad(image, ((1, 2), (1, 2), (0, 0)), mode='reflect')
        if self.pretext_task == "shw_azim_sun_elev":
            pass
        else:
            mask = np.pad(mask, ((1, 2), (1, 2)), mode='reflect')
        return image, mask

    def horizontal_flip(self, image, mask):
        if np.random.rand() < 0.5:
            image = np.flipud(image).copy()
            if self.pretext_task == "shw_azim_sun_elev":
                channel_15 = mask[1]
                channel_15_updated = np.where(channel_15 > 180, 180 - (channel_15 - 180), 360 - channel_15)
                mask[1] = channel_15_updated
            elif self.additional_input == "shw_azim_sun_elev":
                mask = np.flipud(mask).copy()
                channel_15 = image[:, :, 14]
                channel_15_updated = np.where(channel_15 > 0.5, 0.5 - (channel_15 - 0.5), 1 - channel_15)
                image[:, :, 14] = channel_15_updated
            else:
                mask = np.flipud(mask).copy()
        return image, mask

    def vertical_flip(self, image, mask):
        if np.random.rand() < 0.5:
            image = np.fliplr(image).copy()
            if self.pretext_task == "shw_azim_sun_elev":
                channel_15 = mask[1]
                channel_15_updated = np.where(channel_15 > 180, 360 - (channel_15 - 180), 180 - channel_15)
                mask[1] = channel_15_updated
            elif self.additional_input == "shw_azim_sun_elev":
                mask = np.fliplr(mask).copy()
                channel_15 = image[:, :, 14]
                channel_15_updated = np.where(channel_15 > 0.5, 1 - (channel_15 - 0.5), 0.5 - channel_15)
                image[:, :, 14] = channel_15_updated
            else:
                mask = np.fliplr(mask).copy()
        return image, mask

    def random_rotate_90(self, image, mask):
        if np.random.rand() < 0.5:
            image = np.rot90(image, k=1, axes=(1, 0)).copy()
            if self.pretext_task == "shw_azim_sun_elev":
                channel_15 = mask[1]
                channel_15_updated = np.mod(channel_15 - 90, 360)
                mask[1] = channel_15_updated
            elif self.additional_input == "shw_azim_sun_elev":
                mask = np.rot90(mask, k=1, axes=(1, 0)).copy()
                channel_15 = image[:, :, 14]
                channel_15_updated = np.mod(channel_15 - 0.5, 1)
                image[:, :, 14] = channel_15_updated
            else:
                mask = np.rot90(mask, k=1, axes=(1, 0)).copy()
        return image, mask

    def transpose(self, image, mask):
        if np.random.rand() < 0.5:
            image = np.transpose(image, (1, 0, 2)).copy()
            if self.pretext_task == "shw_azim_sun_elev":
                channel_15 = mask[1]
                channel_15_updated_1 = np.mod(channel_15 + 90, 360)
                channel_15_updated = np.where(channel_15_updated_1 > 180, 180 - (channel_15_updated_1 - 180), 360 - channel_15_updated_1)
                mask[1] = channel_15_updated
            elif self.additional_input == "shw_azim_sun_elev":
                mask = np.transpose(mask, (1, 0)).copy()
                channel_15 = image[:, :, 14]
                channel_15_updated_1 = np.mod(channel_15 + 0.5, 1)
                channel_15_updated = np.where(channel_15_updated_1 > 0.5, 0.5 - (channel_15_updated_1 - 0.5), 1 - channel_15_updated_1)
                image[:, :, 14] = channel_15_updated
            else:
                mask = np.transpose(mask, (1, 0)).copy()
        return image, mask

    def apply_transforms(self, image, mask):
        if self.augmentation == "weak_augmentation":
            image, mask = self.pad_if_needed(image, mask)
            image, mask = self.horizontal_flip(image, mask)
            image, mask = self.vertical_flip(image, mask)
            image, mask = self.random_rotate_90(image, mask)
            image, mask = self.transpose(image, mask)
            image = torch.tensor(image, dtype=torch.float32)
            image = image.permute(2, 0, 1)
            mask = torch.tensor(mask, dtype=torch.float32)
            self.global_step = 1
        if self.augmentation == "no_augmentation":
            image, mask = self.pad_if_needed(image, mask)
            image = torch.tensor(image, dtype=torch.float32)
            image = image.permute(2, 0, 1)
            mask = torch.tensor(mask, dtype=torch.float32)
        return image, mask


class TransformWrapper:
    def __init__(self, transformations):
        self.transformations = transformations

    def __call__(self, image, mask):
        return self.transformations.apply_transforms(image, mask)


class SegDataModule(pl.LightningDataModule):
    def __init__(self, data_dir: str, additional_input: str, pretext_task: str):
        super().__init__()
        self.additional_input = additional_input
        self.pretext_task = pretext_task
        self.augmentation = "no_augmentation"
        self.data_dir = data_dir
        self.dataset_metadata = pd.read_csv(self.data_dir + "/cloudsen12_metadata.csv")
        self.dataset_metadata_cloudy = self.dataset_metadata[
             (self.dataset_metadata["cloud_coverage"] != "cloud-free")]
        self.dataset_metadata_high = self.dataset_metadata[self.dataset_metadata["label_type"] == "high"]
        self.batch_size = int(os.getenv("BATCH_SIZE"))
        self.num_worker = int(os.getenv("NUM_WORKER"))

    def setup(self, stage=None):
        TEST_CSV = int(os.getenv("TEST_CSV"))
        world_size = int(os.getenv("DEVICES"))
        rank = self.trainer.local_rank

        if stage == "fit":
            if self.pretext_task == "shw_azim_sun_elev":
                metadata_train_val = self.dataset_metadata_cloudy[self.dataset_metadata_cloudy["test"] != TEST_CSV]
            else:
                metadata_train_val = self.dataset_metadata_high[self.dataset_metadata_high["test"] == 0]
            metadata_train_val.reset_index(drop=True, inplace=True)
            self.metadata_train = metadata_train_val.sample(frac=0.9, random_state=42)
            self.metadata_train.reset_index(drop=True, inplace=True)
            self.metadata_val = metadata_train_val.drop(self.metadata_train.index)
            self.metadata_val.reset_index(drop=True, inplace=True)
            if self.pretext_task == "shw_azim_sun_elev":
                self.augmentation = "weak_augmentation"
                self.train_dataset = DistributedIterableSEGDATASET(self.metadata_train, self.augmentation,
                                                                   self.data_dir, world_size, rank,
                                                                   self.additional_input, self.pretext_task)
            else:
                self.train_dataset = DistributedIterableSEGDATASET(self.metadata_train, "weak_augmentation",
                                                                   self.data_dir, world_size, rank,
                                                                   self.additional_input, self.pretext_task)
            self.val_dataset = DistributedIterableSEGDATASET(self.metadata_val, "no_augmentation",
                                                             self.data_dir, world_size, rank,
                                                             self.additional_input, self.pretext_task)
        if stage == "test":
            self.metadata_test = self.dataset_metadata_high[self.dataset_metadata_high["test"] == TEST_CSV]
            self.metadata_test.reset_index(drop=True, inplace=True)
            self.augmentation = "no_augmentation"
            self.test_dataset = DistributedIterableSEGDATASET(self.metadata_test, self.augmentation,
                                                              self.data_dir, world_size, rank,
                                                              self.additional_input, self.pretext_task)

    def train_dataloader(self):
        return torch.utils.data.DataLoader(dataset=self.train_dataset, batch_size=self.batch_size,
                                           num_workers=self.num_worker, pin_memory=True)

    def val_dataloader(self):
        return torch.utils.data.DataLoader(dataset=self.val_dataset, batch_size=self.batch_size,
                                           num_workers=self.num_worker, pin_memory=True)

    def test_dataloader(self):
        return torch.utils.data.DataLoader(dataset=self.test_dataset, batch_size=self.batch_size,
                                           num_workers=self.num_worker, pin_memory=True)


class DistributedIterableSEGDATASET(IterableDataset):
    def __init__(self, dataset, augmentation, data_dir, world_size, rank, additional_input, pretext_task):
        super(DistributedIterableSEGDATASET, self).__init__()
        self.dataset = dataset
        self.augmentation = augmentation
        self.pretext_task = pretext_task
        self.data_dir = data_dir
        self.world_size = world_size
        self.rank = rank
        self.additional_input = additional_input
        self.partition_dataset()
        self.start = 0
        self.end = len(self.dataset)

    def partition_dataset(self):
        total_size = len(self.dataset)
        per_gpu = int(np.ceil(total_size / float(self.world_size)))
        start_index = per_gpu * self.rank
        end_index = min(start_index + per_gpu, total_size)
        self.dataset = self.dataset.iloc[start_index:end_index]

    def __iter__(self):
        self.max_elevation = None
        self.min_elevation = None
        worker_info = get_worker_info()
        np_transformer = NumpyTransforms(self.augmentation, self.additional_input, self.pretext_task)
        self.augmentation_wrapper = TransformWrapper(np_transformer)

        if worker_info is None:
            iter_start = self.start
            iter_end = self.end
        else:
            per_worker = int(math.ceil((self.end - self.start) / float(worker_info.num_workers)))
            worker_id = worker_info.id
            iter_start = self.start + worker_id * per_worker
            iter_end = min(iter_start + per_worker, self.end)
        if self.additional_input == "shw_azim_sun_elev" or self.pretext_task == "shw_azim_sun_elev":
            # min_azimuth = self.dataset["s2_view_sun_azimuth"].min()
            # max_azimuth = self.dataset["s2_view_sun_azimuth"].max()
            self.min_elevation = self.dataset["s2_view_sun_elevation"].min()
            self.max_elevation = self.dataset["s2_view_sun_elevation"].max()
        for index in range(iter_start, iter_end):
            roi_id = self.dataset.iloc[index]["roi_id"]
            s2_id = self.dataset.iloc[index]["s2_id_gee"]
            normalized_elevation = None
            if self.additional_input == "shw_azim_sun_elev" or self.pretext_task == "shw_azim_sun_elev":
                # sun_azimuth = self.dataset.iloc[index]["s2_view_sun_azimuth"]
                sun_elevation = self.dataset.iloc[index]["s2_view_sun_elevation"]
                if self.max_elevation - self.min_elevation == 0:
                    normalized_elevation = np.zeros_like(sun_elevation)
                else:
                    normalized_elevation = (sun_elevation - self.min_elevation) / (
                                self.max_elevation - self.min_elevation)
            s2l1c = f"{self.data_dir}/high/%s/%s/S2L1C.tif" % (roi_id, s2_id)
            with rio.open(s2l1c) as src:
                inputs = src.read() / 10000
                inputs = np.moveaxis(inputs, 0, -1)
                if self.additional_input == "shw_azim_sun_elev":
                    # normalized_azimuth_array = np.full((509, 509, 1), normalized_azimuth.astype(np.float32))
                    normalized_elevation_array = np.full((509, 509, 1), normalized_elevation.astype(np.float32))
                    # inputs = np.concatenate((inputs, normalized_azimuth_array), axis=2)
                    inputs = np.concatenate((inputs, normalized_elevation_array), axis=2)
            shwdirection = f"{self.data_dir}/high/%s/%s/extra/Shwdirection.tif" % (roi_id, s2_id)
            with rio.open(shwdirection) as src2:
                shw = src2.read() / 100
                if self.additional_input == "shw_azim_sun_elev":
                    shw = shw / 360
                    shw = np.moveaxis(shw, 0, -1)
                    inputs = np.concatenate((inputs, shw.astype(np.float32)), axis=2)
                    manuel_labels = f"{self.data_dir}/high/%s/%s/labels/manual_hq.tif" % (roi_id, s2_id)
                    with rio.open(manuel_labels) as src:
                        target = src.read(1)
                    inputs, target = self.augmentation_wrapper(inputs, target)
                elif self.pretext_task == "shw_azim_sun_elev":
                    pretext_target = np.array([normalized_elevation, shw[0, 0, 0]])
                    inputs, target = self.augmentation_wrapper(inputs, pretext_target)
                    shw_rad = np.radians(target[1])
                    shw_x = np.cos(shw_rad)
                    shw_y = np.sin(shw_rad)
                    target[1] = shw_x
                    shw_y = shw_y.unsqueeze(0)
                    target = torch.cat((target, shw_y), 0)
                else:
                    manuel_labels = f"{self.data_dir}/high/%s/%s/labels/manual_hq.tif" % (roi_id, s2_id)
                    with rio.open(manuel_labels) as src:
                        target = src.read(1)
                    inputs, target = self.augmentation_wrapper(inputs, target)
            if inputs.shape[0] > inputs.shape[2]:
                warnings.warn("segmentation_models.pytorch expects channels first (B, C, H, W)")
            yield inputs, target, "%s__%s" % (roi_id, s2_id)

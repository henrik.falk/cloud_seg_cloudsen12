import os

import pytorch_lightning
import torch
from dotenv import load_dotenv
from pytorch_lightning import Trainer
from pytorch_lightning.loggers import TensorBoardLogger
import segmentation_models_pytorch as smp
from ssl_pretext_experiments.segmodel_module import LitSegModel
from ssl_pretext_experiments.premodel_module import LitPretextModel
from ssl_pretext_experiments.data_module import SegDataModule

load_dotenv()
CONTENT_CLOUDSEN12_PATH = os.getenv("CONTENT_CLOUDSEN12_PATH")
DEVICES = os.getenv("DEVICES")

SEGMODEL = smp.Unet(encoder_name="mobilenet_v2", classes=4, in_channels=15)
model_name = "mobilenet_saa_sza"
epochs = 50
tensorboard_logger = TensorBoardLogger(save_dir="logs", name=model_name + "_e" + str(epochs),
                                       default_hp_metric=False)

model = LitSegModel(SEGMODEL=SEGMODEL)
# model = LitSegModel.load_from_checkpoint("bestmodel/regnet_y_avg_with_pretext_hraug_e5.ckpt")
# model = LitSegModel.load_from_checkpoint("../unet_mobilenetv2/bestmodel/UNetMobV2.ckpt")
# model.pretext_task = "auto_unfreeze"
# model.layer = 19
weights = ""
# model = LitPretextModel()
model.tensorboard_logger = tensorboard_logger
# model = LitPretextModel.load_from_checkpoint("bestmodel/mobilenet_pretext_cosine_max_e50.ckpt")
if weights == "pretext":
    pretrained_state_dict = torch.load("bestmodel/regnet_y_avg_pretext_hraug_e50-v1.ckpt")['state_dict']
    pretrained_state_dict['model.segmentation_head.0.weight'] = torch.randn(4, 16, 3, 3).to(torch.float32)
    pretrained_state_dict['model.segmentation_head.0.bias'] = torch.randn(4).to(torch.float32)
    model.load_state_dict(pretrained_state_dict, strict=False)
    """
    first_three_layers_dict = {k: v for k, v in pretrained_state_dict.items() if
                               k.startswith(('model.encoder.features.1.', 'model.encoder.features.2.'))}
    model.load_state_dict(first_three_layers_dict, strict=False)
    """

    for name, param in model.model.encoder.named_parameters():
        # if name.startswith("features."+str(freeze_before_layer)):
        # break
        # pass
        param.requires_grad = False
    for name, param in model.model.decoder.named_parameters():
        # if name.startswith("features."+str(freeze_before_layer)):
        # break
        # pass
        param.requires_grad = True
else:
    for param in model.model.encoder.parameters():
        param.requires_grad = True
    for name, param in model.model.encoder.named_parameters():
        param.requires_grad = True

dataModule = SegDataModule(CONTENT_CLOUDSEN12_PATH, "shw_azim_sun_elev", "")
callbacks = [pytorch_lightning.callbacks.EarlyStopping(monitor="loss_val", patience=10, mode="min"),
             pytorch_lightning.callbacks.ModelCheckpoint(monitor="loss_val", dirpath="bestmodel", filename=model_name +
             "_e" + str(epochs), save_top_k=2, mode="min")]
trainer = Trainer(max_epochs=epochs, precision=32, accelerator="gpu", devices=DEVICES, callbacks=callbacks,
                  logger=tensorboard_logger, strategy='ddp', num_nodes=1,
                  accumulate_grad_batches=1)
# _find_unused_parameters_true
# pytorch_lightning.seed_everything(42, workers=True)
trainer.fit(model, dataModule)
trainer.test(model, dataModule)
